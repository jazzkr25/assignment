﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RydeApp
{
    public partial class Form1 : Form
    {
        // Jaspreet Kaur c0735945
       // Amanpreet Kaur Bhullar c0735313
       // Neetha Chowdary c0735909
        

        public Form1()
        {
            InitializeComponent();
        }      

        private void buttonBook_Click(object sender, EventArgs e)
        {
            double distance = 0;
            string To = textBoxTo.Text;
            string From = textBoxFrom.Text;
            
            // To check for the valid combinations of To and From Textbox.
            
            if ((From == "Fairview Mall") && (To == "Tim Hortons"))
            {
                distance = 1.2;
                ValidateData(distance, To, From);


            }
            else if ((From == "275 Yorkland Blvd") && (To == "CN Tower"))
            {
                distance = 22.9;
                ValidateData(distance, To, From);


            }
            else if (To == "" || From == "")
            {
                MessageBox.Show("The From Field is Empty.");
            }
            else
            {
                MessageBox.Show("To and From Combination is not valid.");
                textBoxFrom.Text = "";
                textBoxTo.Text = "";

            }

           

           
        }

        // function to validate the data and check whether its pool or direct
        public void ValidateData(double distance, string To, string From)
        {
            string rydeType = "";
            double total = 0;
            double booking = 2.50;
            if (radioButtonPool.Checked == true)
            {

                rydeType = "Ryde Pool";
               
            }
            else if (radioButtonDirect.Checked == true)
            {
                rydeType = "Ryde Direct";
                booking += 2.50*0.2;
                
            }

            else MessageBox.Show("Please select the Ryde Type");

            // function to calculate the total price

            total = Math.Round(Calculate(distance, rydeType),2);
          
            RydeConfirmed screen2 = new RydeConfirmed(To, From, rydeType, total, distance, booking);
            this.Hide();
            screen2.ShowDialog();
       
        }

        public double Calculate(double d, string s)
        {
            double price;
         
            if (s == "Ryde Pool")
            {
                price = 2.50 + (0.81 * d) + 1.75;
                
            }

            else
            {
                // increase base fare by 10 %  and per km price by 15%
               
                price = (2.50 + 2.50 * 0.1) + ((0.81 + 0.81 * 0.15) * d) + 1.75;
            }

            //  if surge is on then increase the fare by 20%
            DateTime now = DateTime.Now;

            if ((Convert.ToDateTime("9:00") <= now && Convert.ToDateTime("12:00") >= now)||
                (Convert.ToDateTime("16:00") <= now && Convert.ToDateTime("18:00") >= now)||
                (Convert.ToDateTime("20:00") <= now && Convert.ToDateTime("21:00") >= now))
            {
                price += price * 0.2;
            }
            // if end price is less than 5.50 set total to 5.50 else return the price
                if (price > 5.50)
                return price;
            else
                return 5.50;
           
        }

       
    }
}

