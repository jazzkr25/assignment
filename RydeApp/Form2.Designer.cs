﻿namespace RydeApp
{
    partial class RydeConfirmed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRydeType = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelBookingFee = new System.Windows.Forms.Label();
            this.labelDistanceCharge = new System.Windows.Forms.Label();
            this.labelServiceFee = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelRydeType
            // 
            this.labelRydeType.AutoSize = true;
            this.labelRydeType.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRydeType.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelRydeType.Location = new System.Drawing.Point(92, 49);
            this.labelRydeType.Name = "labelRydeType";
            this.labelRydeType.Size = new System.Drawing.Size(0, 26);
            this.labelRydeType.TabIndex = 0;
            this.labelRydeType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTo.Location = new System.Drawing.Point(113, 151);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(23, 20);
            this.labelTo.TabIndex = 1;
            this.labelTo.Text = "";
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFrom.Location = new System.Drawing.Point(113, 119);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(41, 20);
            this.labelFrom.TabIndex = 2;
            this.labelFrom.Text = "";
            // 
            // labelBookingFee
            // 
            this.labelBookingFee.AutoSize = true;
            this.labelBookingFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookingFee.Location = new System.Drawing.Point(110, 208);
            this.labelBookingFee.Name = "labelBookingFee";
            this.labelBookingFee.Size = new System.Drawing.Size(0, 20);
            this.labelBookingFee.TabIndex = 3;
            // 
            // labelDistanceCharge
            // 
            this.labelDistanceCharge.AutoSize = true;
            this.labelDistanceCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDistanceCharge.Location = new System.Drawing.Point(110, 243);
            this.labelDistanceCharge.Name = "labelDistanceCharge";
            this.labelDistanceCharge.Size = new System.Drawing.Size(0, 20);
            this.labelDistanceCharge.TabIndex = 4;
            // 
            // labelServiceFee
            // 
            this.labelServiceFee.AutoSize = true;
            this.labelServiceFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServiceFee.Location = new System.Drawing.Point(110, 276);
            this.labelServiceFee.Name = "labelServiceFee";
            this.labelServiceFee.Size = new System.Drawing.Size(0, 20);
            this.labelServiceFee.TabIndex = 5;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotal.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelTotal.Location = new System.Drawing.Point(144, 356);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(0, 26);
            this.labelTotal.TabIndex = 6;
            // 
            // RydeConfirmed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 497);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.labelServiceFee);
            this.Controls.Add(this.labelDistanceCharge);
            this.Controls.Add(this.labelBookingFee);
            this.Controls.Add(this.labelFrom);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.labelRydeType);
            this.Name = "RydeConfirmed";
            this.Text = "RydeConfirmed";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRydeType;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelBookingFee;
        private System.Windows.Forms.Label labelDistanceCharge;
        private System.Windows.Forms.Label labelServiceFee;
        private System.Windows.Forms.Label labelTotal;
    }
}