﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RydeApp
{
    public partial class RydeConfirmed : Form
    {
            // a constructor to display the data on form 2
        public RydeConfirmed(string to, string from, string rydeType, double total, double distance, double Booking)
        {
            InitializeComponent();
            labelRydeType.Text = rydeType + "Confirmed.";
            labelFrom.Text = "From : " + from;
            labelTo.Text   = "To   : " + to;
            labelBookingFee.Text =     "Booking Charge  : $" + Math.Round(Booking,2);
            labelDistanceCharge.Text = "Distance Charge : $" + 0.81*distance;
            labelServiceFee.Text =     "Service Charge   : $1.75";

            labelTotal.Text = "Total : $" + total;
           

        }

        
    }
}
